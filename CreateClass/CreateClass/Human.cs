﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateClass
{
  
    class Human //объект класса
    {
        public string name; //Поле
        public void MyMethod() //Метод
        {
           // public string name; локальная переменная
            Console.WriteLine("Hello, my name is " + name);
        }
}
class Program
{
        static void Main(string[] args)
        {
            Human instance = new Human(); //создается экземпляр класса Human, переменная instance типа Human
            instance.name = "Sergey"; //на экземпляре класса вызываем поле name
            instance.MyMethod(); //на экземпляре класса вызываем метод
            
            Car instance2 = new Car();
            Console.WriteLine("Введите марку и объем двигателя автомобиля");
            instance2.Model = Console.ReadLine();
            instance2.Volume = Console.ReadLine();
            instance2.MyMethod2();
            Console.ReadKey();
        }
    }

    
}
