using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsingPropertiesInClasses
{
    class MyClass
    {
        private string field = null;
        public string Field //открытое строковое свойство в теле которого методы
        {
            set //метод-мутатор (setter)
            {
                field = value;
            }
            get //метод-аксессор (getter)
            {
                return field; //SuperComment for SuperBranch
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            MyClass instance = new MyClass();
            instance.Field = "Hello";           //метод-мутатор (setter), срабатывает метод set
            // значение "Hello" передаётся в value и присваивается в field.
            Console.WriteLine(instance.Field); // метод-аксессор (getter) срабатывает когда мы получаем
            //какое-то значение
            //Readonly если отсутствует метод set, Writeonly если отсутствует метод get
            Console.ReadKey();
        }
    }
}
